FROM ubuntu:latest
COPY ./gcc_script.exp /root/
COPY ./amiro_init.exp /root/
RUN /bin/sh -c "apt-get update && export DEBIAN_FRONTEND=noninteractive && apt-get install -y --no-install-recommends tzdata && apt-get install -y expect bash git build-essential cmake wget lsb-release software-properties-common gnupg && echo \"export PS1='\w:'\" >> ~/.bashrc"
RUN /bin/bash -l -c "export SHELL=/bin/bash && export TERM=xterm && cd && expect ./gcc_script.exp"
RUN /bin/bash -l -c "wget https://apt.llvm.org/llvm.sh && chmod +x llvm.sh && ./llvm.sh 17 && apt-get install clang-tidy-17 -y"
